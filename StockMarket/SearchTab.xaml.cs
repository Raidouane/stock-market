﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using StockMarket.models;
using Xamarin.Forms;
using System.Globalization;
using System.Linq;

namespace StockMarket
{
    public partial class SearchTab : ContentPage
    {
        public SearchTab()
        {
            InitializeComponent();
            ResizeElements();
        }

        private void ResizeElements()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                SearchContainer.Padding = new Thickness(10);
                Symbol.HeightRequest = 100;
            }
         }

        async void HandleRefreshing(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(Symbol.Text))
            {
                StockListView.IsRefreshing = false;
                return;
            }

            await FetchByStockSymbolAsync(Symbol.Text);
            StockListView.IsRefreshing = false;
        }

        private void SetHighest(ObservableCollection<Stock> stocks)
        {
            var stockList = new List<Stock>(stocks);
            double highest = stockList.Max(stock => double.Parse(stock.HighPrice, CultureInfo.InvariantCulture.NumberFormat));

            Highest.Text = "$" + highest.ToString();
        }

        private void SetLowest(ObservableCollection<Stock> stocks)
        {
            var stockList = new List<Stock>(stocks);
            double lowest = stockList.Min(stock => double.Parse(stock.LowPrice, CultureInfo.InvariantCulture.NumberFormat));

            Lowest.Text = "$" + lowest.ToString();
        }

        private async System.Threading.Tasks.Task FetchByStockSymbolAsync(string symbol)
        {
            try
            {
                Dictionary<string, Stock> stocksFromApi = await App.StockManager.RefreshDataAsync(symbol);
                if (stocksFromApi == null)
                {
                    throw new Exception("Retry Later");
                }
                var stocks = new ObservableCollection<Stock>();
                foreach (var stockFromApi in stocksFromApi)
                {
                    Stock stock = new Stock(stockFromApi.Value)
                    {
                        Date = stockFromApi.Key
                    };
                    stocks.Add(stock);
                }
                StockHeader.IsVisible = true;
                SetHighest(stocks);
                SetLowest(stocks);
                StockListView.ItemsSource = stocks;
            }
            catch (Exception e)
            {
                StockListView.ItemsSource = new ObservableCollection<Stock>();
                StockHeader.IsVisible = false;
                await DisplayAlert("Error", e.Message, "Close");
            }
        }

        async void HandleValidateSearch(object sender, EventArgs args)
        {
            await FetchByStockSymbolAsync(Symbol.Text);
        }
    }
}
