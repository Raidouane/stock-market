﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StockMarket.models;

namespace StockMarket.data
{
    public interface IRestService
    {
        Task<Dictionary<string, Stock>> RefreshDataAsync(string stockSymbol);
        Dictionary<string, Stock> GetStocks();
    }
}
