﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using StockMarket.models;

namespace StockMarket.data
{
    public class RestService : IRestService
    {
        HttpClient _client;

        public Dictionary<string, Stock> Stocks
        {
            get;
            private set;
        }


        public RestService()
        {
            _client = new HttpClient();
            _client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<Dictionary<string, Stock>> RefreshDataAsync(string stockSymbol = "AAPL")
        {
            Stocks = new Dictionary<string, Stock>();
            var uri = new Uri(string.Format(Constants.AlphavantageUrlTimeSeriesDaily, Constants.AlphavantageAPIKey, stockSymbol));

            var response = await _client.GetAsync(uri);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Please retry later.");
            }

            var content = await response.Content.ReadAsStringAsync();
            AlphavantageResponse resp = AlphavantageResponse.FromJson(content);
            if (resp.ErrorMessage != null)
            {
                throw new Exception("The searched symbol was not found.");
            }

            Debug.WriteLine(@"ERROR {0}", resp.Content);
            Stocks = resp.Content;
            return Stocks;
        }

        public Dictionary<string, Stock> GetStocks()
        {
            return Stocks;
        }
    }
}
