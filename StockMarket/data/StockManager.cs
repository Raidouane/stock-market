﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StockMarket.models;

namespace StockMarket.data
{
    public class StockManager
    {
        IRestService _restService;

        public StockManager(IRestService service)
        {
            _restService = service;
        }

        public Task<Dictionary<string, Stock>> RefreshDataAsync(string stockSymbol)
        {
            return _restService.RefreshDataAsync(stockSymbol);
        }

        public Dictionary<string, Stock> GetStocks()
        {
            var stocks = _restService.GetStocks();
            return stocks ?? new Dictionary<string, Stock>();
        }
    }
}
