﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;

namespace StockMarket
{
    public partial class AnaliticsPage : ContentPage
    {
        public AnaliticsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Dictionary<string, models.Stock> stocks = App.StockManager.GetStocks();
            if (stocks == null || stocks.Count == 0)
            {
                DisplayNoDataLabel();
                return;
            }

            DisplayMicrochartsBlock();
            CreateBarChart();
            CreateLineChart();
        }

        private void DisplayNoDataLabel()
        {
            NoDataLabel.IsVisible = true;
            MicrochartsBlock.IsVisible = false;
        }

        private void DisplayMicrochartsBlock()
        {
            NoDataLabel.IsVisible = false;
            MicrochartsBlock.IsVisible = true;
        }

        private List<Microcharts.Entry> GetEntriesWithMinAndMax(List<Microcharts.Entry> entriesTOShow)
        {
            if (entriesTOShow == null || entriesTOShow.Count == 0)
            {
                return new List<Microcharts.Entry>();
            }
                        float highestValue = entriesTOShow.Max(e => e.Value);
            Microcharts.Entry highestEntry = entriesTOShow.First(e => Math.Abs(e.Value - highestValue) < 1);
            int index = entriesTOShow.IndexOf(highestEntry);
            if (index != -1)
                entriesTOShow[index] = new Microcharts.Entry(highestEntry.Value)
                {
                    ValueLabel = highestEntry.ValueLabel,
                    Color = SKColor.Parse("#4bb543")

                };
            float lowestValue = entriesTOShow.Min(e => e.Value);
            Microcharts.Entry lowestEntry = entriesTOShow.First(e => Math.Abs(e.Value - lowestValue) < 0.5);
            int lowestIndex = entriesTOShow.IndexOf(lowestEntry);
            if (lowestIndex != -1)
                entriesTOShow[lowestIndex] = new Microcharts.Entry(lowestEntry.Value)
                {
                    ValueLabel = lowestEntry.ValueLabel,
                    Color = SKColor.Parse("#cb1719")

                };

            return entriesTOShow;
        }

        private void CreateLineChart()
        {
            Dictionary<string, models.Stock> stocks = App.StockManager.GetStocks();
            List<Microcharts.Entry> entryList = new List<Microcharts.Entry>();
            int i = 0;
            foreach (KeyValuePair<string, models.Stock> stock in stocks)
            {
                float highPrice = float.Parse(stock.Value.HighPrice, CultureInfo.InvariantCulture.NumberFormat);
                Microcharts.Entry stat = new Microcharts.Entry(highPrice)
                {
                    Color = SKColor.Parse("#4db2ff")
                };
                if (i % 10 == 0)
                {
                    stat.ValueLabel = stock.Value.HighPrice;
                }
                entryList.Add(stat);
                i++;
            }
            List<Microcharts.Entry> entries = GetEntriesWithMinAndMax(entryList);
            LineChart lineChart = new LineChart()
            {
                Entries = entries
            };
            LineChart.Chart = lineChart;
        }

        private void CreateBarChart()
        {
            Dictionary<string, models.Stock> stocks = App.StockManager.GetStocks();
            List<Microcharts.Entry> entryList = new List<Microcharts.Entry>();
            foreach (KeyValuePair<string, models.Stock> stock in stocks)
            {
                float highPrice = float.Parse(stock.Value.HighPrice, CultureInfo.InvariantCulture.NumberFormat);
                Microcharts.Entry stat = new Microcharts.Entry(highPrice)
                {
                    ValueLabel = stock.Value.HighPrice,
                    Color = SKColor.Parse("#4db2ff")

                };
                entryList.Add(stat);
            }
            List<Microcharts.Entry> entries = GetEntriesWithMinAndMax(new List<Microcharts.Entry>(entryList.Take(30)));
            BarChart BarChart = new BarChart()
            {
                Entries = entries,
            };
            Chart.Chart = BarChart;
        }
    }
}
