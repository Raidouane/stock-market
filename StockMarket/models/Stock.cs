﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace StockMarket.models
{
    public partial class Stock
    {
        [JsonProperty("1. open")]
        public string OpenPrice { get; set; }

        [JsonProperty("2. high")]
        public string HighPrice { get; set; }

        [JsonProperty("3. low")]
        public string LowPrice { get; set; }

        [JsonProperty("4. close")]
        public string ClosePrice { get; set; }

        [JsonProperty("5. volume")]
        public string Volume { get; set; }

        public string Date { get; set; }

        public Stock(Stock stock)
        {
            if (stock == null) return;

            OpenPrice = stock.OpenPrice;
            HighPrice = stock.HighPrice;
            LowPrice = stock.LowPrice;
            ClosePrice = stock.ClosePrice;
            Volume = stock.Volume;
            Date = stock.Date;
        }
    }

    public partial class Stock
    {
        public static Dictionary<string, Stock> FromJson(string json) => JsonConvert.DeserializeObject<Dictionary<string, Stock>>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Dictionary<string, Stock> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
