﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace StockMarket.models
{
    public partial class AlphavantageResponse
    {
        [JsonProperty("Meta Data")]
        public AlphavantageMetaDataResponse MetaData { get; set; }

        [JsonProperty("Time Series (Daily)")]
        public Dictionary<string, Stock> Content { get; set; }

        [JsonProperty("Error Message")]
        public String ErrorMessage { get; set; }
    }

    public partial class AlphavantageResponse
    {
        public static AlphavantageResponse FromJson(string json) => JsonConvert.DeserializeObject<AlphavantageResponse> (json, Converter.Settings);
    }
}
