﻿using System;
using StockMarket.data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace StockMarket
{
    public partial class App : Application
    {
        public static StockManager StockManager { get; private set; }

        public App()
        {
            InitializeComponent();
            StockManager = new StockManager(new RestService());
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
